package io.github.gjosiah.tdm.util;

import java.io.File;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;
import org.bukkit.generator.ChunkGenerator;

public class WorldUtil {

    public static final ChunkGenerator voidgen = new VoidGenerator();
    
    
    public static World loadWorld(String name, boolean create) {
        return create ? createWorld(name) : findWorld(name);
    }
    
    public static World findWorld(String name) {
        return new File(Bukkit.getWorldContainer(), name).isDirectory() ? new WorldCreator(name).createWorld() : null;
    }
    
    public static World createWorld(String name) {
        return new WorldCreator(name).generator(voidgen).environment(World.Environment.NORMAL)
                .generateStructures(false).type(WorldType.NORMAL).createWorld();
    }
    
    public static class VoidGenerator extends ChunkGenerator {
    
        public byte[][] generateBlockSections(World world, Random random, int x, int z, BiomeGrid biomes) {
            return new byte[world.getMaxHeight() / 16][];
        }
    }
}
