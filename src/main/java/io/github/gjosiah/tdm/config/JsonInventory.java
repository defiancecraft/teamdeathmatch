package io.github.gjosiah.tdm.config;

import io.github.gjosiah.tdm.config.JsonInventory.JsonInventorySlot;
import io.github.gjosiah.tdm.util.ArrayUtil;
import io.github.gjosiah.tdm.util.ItemStackUtil;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Repairable;

import com.google.common.collect.Iterators;

public class JsonInventory implements Iterable<JsonInventorySlot> {

    private JsonInventorySlot[] slots;
    
    public JsonInventory(JsonInventorySlot... slots) {
        this.slots = slots == null ? new JsonInventorySlot[0] : slots;
    }
    
    public JsonInventory() {
        this(new JsonInventorySlot[0]);
    }
    
    public Iterator<JsonInventorySlot> iterator() {
        return Iterators.forArray(slots);
    }
    
    public JsonInventorySlot[] getSlots() {
        return slots.clone();
    }
    
    
    public static class JsonInventorySlot {
    
        private int index;
        private int amount;
        private String material;
        private short data;
        private String display;
        private String[] lore;
        private Enchant[] enchants;
        
        @SuppressWarnings("deprecation")
        public JsonInventorySlot(int index, ItemStack item) {
            this.index = index;
            this.amount = item.getAmount();
            this.material = item == null ? null : item.getType().name();
            this.data = item == null ? null : item.getData().getData();
            this.display = item == null ? null : item.getItemMeta() == null ? null : item.getItemMeta().getDisplayName();
            List<String> lorelist = item == null ? null : item.getItemMeta() == null ? null : item.getItemMeta().getLore();
            this.lore = lorelist == null ? null : lorelist.toArray(new String[lorelist.size()]);
            Map<Enchantment, Integer> onitem = item.getEnchantments();
            this.enchants = new Enchant[0];
            
            for (Map.Entry<Enchantment, Integer> e : onitem.entrySet()) {
                Enchant newenc = new Enchant();
                newenc.enchantment = e.getKey().getName();
                newenc.level = e.getValue().intValue();
                this.enchants = ArrayUtil.addElement(enchants, newenc);
            }
        }
        
        public JsonInventorySlot() {
            this.index = -1;
            this.amount = 0;
            this.material = "";
            this.data = -1;
            this.display = null;
            this.lore = new String[0];
            this.enchants = new Enchant[0];
        }
        
        public int getIndex() {
            return index;
        }
        
        public ItemStack getItem() {
            Material m;
            
            if (material == null || (m = Material.valueOf(material.toUpperCase())) == null) {
                throw new IllegalArgumentException("No Material has been configured for this inventory slot!");
            }
            
            ItemStack item = new ItemStack(m, amount);
            applyData(item, data);
            
            if (enchants != null) {
                for (Enchant e : enchants) {
                    Enchantment enchantment = Enchantment.getByName(e.enchantment);
                    
                    if (enchantment != null) {
                        item.addUnsafeEnchantment(enchantment, e.level);
                    }
                }
            }
            
            return ItemStackUtil.modify(item, display, lore);
        }
        
        public void addToInventory(Inventory inv) {
            if (index < 0) {
                throw new IllegalArgumentException("No valid index has been configured for this inventory slot!");
            }
            
            if (index >= inv.getSize()) {
                return;
            }
            
            inv.setItem(getIndex(), getItem());
        }
    }
    
    @SuppressWarnings("deprecation")
    private static void applyData(ItemStack item, short data) {
        if (data < 0 ) {
            return;
        }
        
        switch (item.getType()) {
        case AIR:
            return;
        default:
            if (item.getItemMeta() instanceof Repairable) {
                item.setDurability(data);
            } else {
                item.getData().setData((byte) data);
            }
        }
    }
    
    public static class Enchant {
    
        public String enchantment;
        public int level;
    }
}
