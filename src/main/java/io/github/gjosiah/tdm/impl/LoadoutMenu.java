package io.github.gjosiah.tdm.impl;

import com.archeinteractive.defiancetools.util.ui.Menu;
import io.github.gjosiah.tdm.TeamDeathmatch;
import io.github.gjosiah.tdm.util.SimpleMenuItem;

import org.bukkit.entity.Player;

public class LoadoutMenu extends Menu {

    private static final int[] LOADOUT_SLOTS = {
        //     Loadouts
        // L O L O L O L O L
        // L O L O L O L O L
        // 
        // O - Open slot
        // L - Loadout item
        0, 2, 4, 6, 8, 11, 13, 15, 17
    };
    
    private final Arena arena;
    
    public LoadoutMenu(Arena arena) {
        super("Loadouts", 2);
        
        this.arena = arena;
        Loadout[] loadouts = arena.getLoadouts();
        
        for (int i = 0; i < Math.min(loadouts.length, LOADOUT_SLOTS.length); i++) {
            Loadout loadout = loadouts[i];
            
            if (loadout == null) {
                continue;
            }
            
            addMenuItem(new SimpleMenuItem(loadout.icon, player -> equip(player, loadout)), getLoadoutSlot(i));
        }
    }
    
    public void checkDefault(Player player) {
        GamePlayer gp = arena.getPlayer(player);
        
        if (gp != null && gp.loadout == null) {
            gp.loadout = ArenaManager.getLoadout(arena.getLoadouts(), player);
            
            if (gp.loadout == null) {
                return;
            }
            
            arena.equipLoadout(player, gp.loadout);
            arena.replacejoining(player, player.getInventory().getContents().clone());
        }
    }
    
    public Arena getArena() {
        return arena;
    }
    
    private void equip(Player player, Loadout loadout) {
        if (TeamDeathmatch.getInstance().hasPermission(player, loadout.permission)) {
            arena.equipLoadout(player, loadout);
            arena.replacejoining(player, player.getInventory().getContents().clone());
        } else {
            player.sendMessage(loadout.perm_message);
        }
    }
    
    
    public static int getLoadoutSlot(int index) {
        if (index < 0 || index >= LOADOUT_SLOTS.length) {
            return -1;
        }
        
        return LOADOUT_SLOTS[index];
    }
}
