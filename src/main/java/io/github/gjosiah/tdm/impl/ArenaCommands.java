package io.github.gjosiah.tdm.impl;

import io.github.gjosiah.tdm.TeamDeathmatch;
import io.github.gjosiah.tdm.config.ArenaChest;
import io.github.gjosiah.tdm.config.ArenaConfig;
import io.github.gjosiah.tdm.config.ArenaSign;
import io.github.gjosiah.tdm.config.JsonInventory;
import io.github.gjosiah.tdm.config.LoadoutInfo;
import io.github.gjosiah.tdm.config.TeamDeathmatchConfig;
import io.github.gjosiah.tdm.util.SchematicPasteTask;
import io.github.gjosiah.tdm.util.WorldUtil;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

public class ArenaCommands {

    private static final String INSUFFICIENT_PERMISSION = ChatColor.RED + "You don't have permission to use this command.";
    
    
    public static boolean schematicpaste(Player player, String[] args) {
        if (!player.isOp()) {
            return false;
        }
        
        if (args.length < 1) {
            player.sendMessage("/schpaste <path_to_schematic> [blockrate] [locationx, locationy, locationz]");
            return false;
        }
        
        File schematic = new File(args[0]);
        
        if (!schematic.isFile()) {
            player.sendMessage("The given file path " + (schematic.exists() ? "represents a directory!" : "does not exist!"));
            return false;
        }
        
        int pertick = 50;
        
        if (args.length > 1) {
            try {
                pertick = Integer.parseInt(args[1]);
            } catch (NumberFormatException e) {
                player.sendMessage("Could not parse block per tick count " + args[1] + " into an integer!");
                return false;
            }
        }
        
        Vector paste = player.getLocation().toVector();
        
        if (args.length > 4) {
            try {
                double x = Double.parseDouble(args[2]);
                double y = Double.parseDouble(args[3]);
                double z = Double.parseDouble(args[4]);
                paste = new Vector(x, y, z);
            } catch (NumberFormatException e) {
                player.sendMessage(String.format("Could not parse (%s, %s, %s) into a coordinate!", args[2], args[3], args[4]));
                return false;
            }
        }
        
        player.sendMessage("Initiating schematic paste...");
        new SchematicPasteTask(paste.toLocation(player.getWorld()), schematic, pertick).runTaskTimer(TeamDeathmatch.getInstance(), 1L, 1L);
        return true;
    }
    
    public static boolean listworlds(CommandSender sender, String[] args) {
        if (!sender.isOp()) {
            return false;
        }
        
        Bukkit.getWorlds().forEach(world -> sender.sendMessage(world.getName()));
        return true;
    }
    
    public static boolean listarenas(CommandSender sender, String[] args) {
        if (!sender.isOp()) {
            return false;
        }
        
        TeamDeathmatch.getInstance().getArenas().forEach(entry -> {
            Arena arena = entry.getValue();
            sender.sendMessage(String.format("Arena \"%s\":", entry.getKey()));
            sender.sendMessage(String.format("  Using World '%s' (%s)", arena.getWorldName(), arena.isWorldLoaded() ? "is loaded" : "not loaded"));
            sender.sendMessage(String.format("  Using schematic at %s", arena.getSchematic().getAbsolutePath()));
            sender.sendMessage(String.format("  In phase %s", arena.getPhase().name()));
            sender.sendMessage("");
        });
        
        return true;
    }
    
    public static boolean loadworld(CommandSender sender, String[] args) {
        if (!sender.isOp()) {
            return false;
        }
        
        if (args.length > 0) {
            sender.sendMessage("Attempting to load or create the World of name '" + args[0] + "'.");
            WorldUtil.createWorld(args[0]);
            return true;
        } else {
            sender.sendMessage("/loadworld <name>");
            return false;
        }
    }
    
    public static boolean tdm(Player player, String[] args) {
        if (args.length < 1) {
            player.sendMessage("/tdm <join/leave/info/config> [options]");
            return false;
        }
        
        Arena arena = TeamDeathmatch.getInstance().getArenas().getArena(player);
        
        switch (args[0].toLowerCase()) {
        case "join":
            if (!player.isOp() && !TeamDeathmatch.getInstance().hasPermission(player, "tdm.join")) {
                player.sendMessage(INSUFFICIENT_PERMISSION);
                return false;
            }
            
            if (args.length < 2) {
                player.sendMessage("/tdm join <arena>");
                return false;
            }
            
            if (arena != null) {
                player.sendMessage("You are already in the arena '" + arena.getLabel() + "'!");
                return false;
            }
            
            TeamDeathmatch.getInstance().getArenas().sendToArena(args[1], player);
            return true;
        case "leave":
            if (!player.isOp() && !TeamDeathmatch.getInstance().hasPermission(player, "tdm.leave")) {
                player.sendMessage(INSUFFICIENT_PERMISSION);
                return false;
            }
            
            if (arena == null) {
                player.sendMessage("You are not currently in any arena!");
                return false;
            }
            
            TeamDeathmatch.getInstance().getArenas().removeFromArena(player);
            return true;
        case "info":
            if (!player.isOp() && !TeamDeathmatch.getInstance().hasPermission(player, "tdm.info")) {
                player.sendMessage(INSUFFICIENT_PERMISSION);
                return false;
            }
            
            player.sendMessage(arena == null ? "You are not currently in an arena." : "You are currently in arena '" + arena.getLabel() + "'.");
            return true;
        case "config":
            if (!player.isOp() && !TeamDeathmatch.getInstance().hasPermission(player, "tdm.config")) {
                player.sendMessage(INSUFFICIENT_PERMISSION);
                return false;
            }
            
            return configure(player, Arrays.copyOfRange(args, 1, args.length));
        default:
            player.sendMessage("Unknown option " + args[0] + ".");
            return false;
        }
    }
    
    private static boolean configure(Player player, String[] args) {
        if (args.length < 1) {
            player.sendMessage("TDM Configuration commands:");
            player.sendMessage("/tdm config inv <filepath>\n"
                    + " -- save a target inventory to file.");
            player.sendMessage("/tdm config link <kit/chest> <filepath>\n"
                    + " -- link a saved inventory file to your current arena as a chest or a loadout kit.");
            player.sendMessage("/tdm config chestlink <filepath>\n"
                    + " -- save a target inventory to file and link it as a chest in your current arena.");
            player.sendMessage("/tdm config kitlink <filepath>\n"
                    + " -- save a target inventory to file and add it onto the kits for your current arena.");
            player.sendMessage("/tdm config joinsign <arena>\n"
                    + "/tdm config quitsign <arena>\n"
                    + "/tdm config kitsign <arena>\n"
                    + " -- save a target sign to the config as a sign for the specified arena.");
            player.sendMessage("/tdm config spawnloc <red/blue>\n"
                    + " -- save your location as a spawn for the given team in your current arena.");
            return false;
        }
        
        Arena arena = getArena(player);
        
        switch (args[0].toLowerCase()) {
        case "inv": {
            return saveInventory(player, args);
        }
        case "link": {
            return linkInventory(player, args, arena);
        }
        case "chestlink": {
            if (arena == null) {
                player.sendMessage("You must be in an arena to use this command.");
                player.sendMessage("Use \"/tdm join <arena name>\" to join an arena.");
                return false;
            }
            
            // the value in args[1] matches (the filepath argument) so we can just use the same array
            if (!saveInventory(player, args)) {
                return false;
            }
            
            String[] linkargs = { "link", "chest", args[1] };
            
            if (!linkInventory(player, linkargs, arena)) {
                return false;
            }
            
            return true;
        }
        case "kitlink": {
            // the value in args[1] matches (the filepath argument) so we can just use the same array
            if (!saveInventory(player, args)) {
                return false;
            }
            
            String[] linkargs = { "link", "kit", args[1] };
            
            if (!linkInventory(player, linkargs, arena)) {
                return false;
            }
            
            return true;
        }
        case "joinsign": {
            return addSign(true, player, args);
        }
        case "quitsign": {
            return addSign(false, player, args);
        }
        case "kitsign": {
            return kitSign(player, args);
        }
        case "spawnloc": {
            if (args.length < 2) {
                player.sendMessage("/tdm config spawnloc <red/blue>");
                return false;
            }
            
            if (arena == null) {
                player.sendMessage("You must be in an arena to use this command.");
                player.sendMessage("Use \"/tdm join <arena name>\" to join an arena.");
                return false;
            }
            
            try {
                Team team = Team.valueOf(args[1].toUpperCase());
                ArenaConfig.TeamSpawn spawn;
                
                switch (team) {
                case RED:
                    spawn = new ArenaConfig.TeamSpawn("red", player.getLocation());
                    break;
                case BLUE:
                    spawn = new ArenaConfig.TeamSpawn("blue", player.getLocation());
                    break;
                default:
                    player.sendMessage("Unrecognized team " + team.name());
                    return false;
                }
                
                TeamDeathmatchConfig config = TeamDeathmatch.getInstance().getConfiguration();
                TeamDeathmatchConfig.addSpawn(config, arena.getLabel(), spawn);
                player.sendMessage("Successfully added a " + team.name() + " spawn at your location for the arena " + arena.getLabel() + ".");
                return true;
            } catch (Exception e) {
                player.sendMessage("Unrecognized team '" + args[1] + "'!");
                return false;
            }
        }
        default:
            player.sendMessage("Unknown option '" + args[0] + "'.");
            return false;
        }
    }
    
    private static boolean kitSign(Player player, String[] args) {
        if (args.length < 2) {
            player.sendMessage("/tdm config kitsign <arena label>");
            return false;
        }
        
        String label;
        boolean force = false;
        
        if (args.length < 2) {
            player.sendMessage("/tdm config kitsign <arena label>");
            return false;
        } else if (args.length < 3 && "-f".equals(args[1])) {
            player.sendMessage("/tdm config kitsign -f <arena>");
            return false;
        } else if (args.length > 2) {
            if ("-f".equalsIgnoreCase(args[1])) {
                force = true;
                label = args[2];
            } else {
                label = args[1];
            }
        } else {
            label = args[1];
        }
        
        ArenaConfig aconfig = TeamDeathmatch.getInstance().getArenas().getArenaConfig(label);
        
        if (aconfig == null) {
            player.sendMessage("Cannot find the arena specified!");
            return false;
        }
        
        if (force || TeamDeathmatch.getInstance().getArenas().hasArena(label)) {
            @SuppressWarnings("deprecation")
            Block target = player.getTargetBlock(null, 100);
            
            if (target != null && target.getState() instanceof Sign) {
                ArenaSign sign = new ArenaSign(label, "", target.getWorld().getName(), target.getX(), target.getY(), target.getZ());
                TeamDeathmatchConfig.addKitSign(TeamDeathmatch.getInstance().getConfiguration(), sign);
                
                player.sendMessage("Successfully added a kit sign at " + target.getLocation());
                return true;
            } else {
                player.sendMessage("You must have a sign on your cursor when using this command.");
                return false;
            }
        } else {
            player.sendMessage("The arena indicated (" + label + ") does not exist.");
            player.sendMessage("Use \"/tdm config kitsign -f <arena>\" to suppress this warning.");
            return false;
        }
    }
    
    private static boolean addSign(boolean join, Player player, String[] args) {
        String label;
        boolean force = false;
        
        if (args.length < 2) {
            player.sendMessage("/tdm config joinsign <arena label>");
            return false;
        } else if (args.length < 3 && "-f".equalsIgnoreCase(args[1])) {
            player.sendMessage("/tdm config joinsign -f <arena>");
            return false;
        } else if (args.length > 2) {
            if ("-f".equalsIgnoreCase(args[1])) {
                force = true;
                label = args[2];
            } else {
                label = args[1];
            }
        } else {
            label = args[1];
        }
        
        if (force || TeamDeathmatch.getInstance().getArenas().hasArena(label)) {
            @SuppressWarnings("deprecation")
            Block target = player.getTargetBlock(null, 100);
            
            if (target != null && target.getState() instanceof Sign) {
                ArenaSign sign = new ArenaSign(label, "", target.getWorld().getName(), target.getX(), target.getY(), target.getZ());
                
                if (join) {
                    TeamDeathmatchConfig.addJoinSign(TeamDeathmatch.getInstance().getConfiguration(), sign);
                } else {
                    TeamDeathmatchConfig.addQuitSign(TeamDeathmatch.getInstance().getConfiguration(), sign);
                }
                
                player.sendMessage("Successfully added a " + (join ? "join" : "quit") + " sign at " + target.getLocation());
                return true;
            } else {
                player.sendMessage("You must have a sign on your cursor when using this command.");
                return false;
            }
        } else {
            player.sendMessage("The arena indicated (" + label + ") does not exist.");
            player.sendMessage("Use \"/tdm config joinsign -f <arena>\" to suppress this warning.");
            return false;
        }
    }
    
    private static Arena getArena(Player player) {
        return TeamDeathmatch.getInstance().getArenas().getArena(player);
    }
    
    private static boolean saveInventory(Player player, String[] args) {
        String path;
        boolean force = false;
        
        if (args.length < 2) {
            player.sendMessage("/tdm config inv <filepath>");
            return false;
        } else if (args.length < 3 && "-f".equalsIgnoreCase(args[1])) {
            player.sendMessage("/tdm config inv -f <filepath>");
            return false;
        } else if (args.length > 2) {
            if ("-f".equalsIgnoreCase(args[1])) {
                force = true;
                path = args[2];
            } else {
                path = args[1];
            }
        } else {
            path = args[1];
        }
        
        File save = new File(TeamDeathmatch.getInstance().getDataFolder(), path);
        
        if (save.exists() && !force) {
            player.sendMessage("The file denoted by the given path already exists!");
            player.sendMessage("Use \"/tdm config inv -f <filepath>\" to overwrite this file.");
            return false;
        } else {
            if (save.getParentFile() != null) {
                save.getParentFile().mkdirs();
            }
        }
        
        @SuppressWarnings("deprecation")
        Block target = player.getTargetBlock(null, 100);
        
        if (target != null && target.getState() instanceof InventoryHolder) {
            Inventory inv = ((InventoryHolder) target.getState()).getInventory();
            List<JsonInventory.JsonInventorySlot> slots = new ArrayList<>();
            
            for (int i = 0; i < inv.getSize(); i++) {
                ItemStack item = inv.getItem(i);
                
                if (item != null && !Material.AIR.equals(item.getType())) {
                    slots.add(new JsonInventory.JsonInventorySlot(i, item));
                }
            }
            
            try (FileWriter writer = new FileWriter(save)) {
                TeamDeathmatch.GSON.toJson(new JsonInventory(slots.toArray(new JsonInventory.JsonInventorySlot[slots.size()])), writer);
                player.sendMessage("Successfully saved target inventory to a file at " + save.getAbsolutePath());
                return true;
            } catch (IOException e) {
                player.sendMessage("Unable to write to file successfully!");
                e.printStackTrace();
                return false;
            }
        } else {
            player.sendMessage("You must have an inventory block on your cursor when using this command.");
            return false;
        }
    }
    
    private static boolean linkInventory(Player player, String[] args, Arena arena) {
        boolean kit;
        String path;
        
        if (args.length < 3) {
            player.sendMessage("/tdm config link <kit/chest> <filepath>");
            return false;
        } else {
            switch (args[1].toLowerCase()) {
            case "kit":
                kit = true;
                break;
            case "chest":
                kit = false;
                break;
            default:
                player.sendMessage("Unknown option '" + args[1] + "'.");
                return false;
            }
            
            path = args[2];
        }
        
        File link = new File(TeamDeathmatch.getInstance().getDataFolder(), path);
        
        if (!link.exists()) {
            player.sendMessage("The file denoted by the given path does not exist!");
            return false;
        }
        
        if (arena == null) {
            player.sendMessage("You must be in an arena to use this command.");
            player.sendMessage("Use \"/tdm join <arena name>\" to join an arena.");
            return false;
        }
        
        TeamDeathmatchConfig config = TeamDeathmatch.getInstance().getConfiguration();
        ArenaConfig aconfig = TeamDeathmatch.getInstance().getArenas().getArenaConfig(arena);
        
        if (kit) {
            LoadoutInfo loadout = new LoadoutInfo(new JsonInventory.JsonInventorySlot(), "", path, "", "");
            ArenaConfig.addLoadout(config, aconfig, loadout);
            player.sendMessage("Successfully linked a loadout to the file at " + link.getAbsolutePath());
            return true;
        } else {
            @SuppressWarnings("deprecation")
            Block target = player.getTargetBlock(null, 100);
            
            if (target != null && target.getState() instanceof InventoryHolder) {
                ArenaChest chest = new ArenaChest(path, target.getLocation());
                TeamDeathmatchConfig.addChest(config, arena.getLabel(), chest);
                player.sendMessage("Successfully linked a chest to the file at " + link.getAbsolutePath());
                return true;
            } else {
                player.sendMessage("You must have an inventory block on your cursor when using this command.");
                return false;
            }
        }
    }
}
