package io.github.gjosiah.tdm.config;

import java.util.Objects;

import com.archeinteractive.minigameapi.config.JsonConfig;
import io.github.gjosiah.tdm.TeamDeathmatch;
import io.github.gjosiah.tdm.util.ArrayUtil;

import org.bukkit.ChatColor;

public class TeamDeathmatchConfig extends JsonConfig {

    private static final String DEFAULT_GAMENAME = "Team Deathmatch";
    
    private Game game;
    private Messages messages;
    private LoadoutInfo[] loadouts;
    private ArenaConfig[] arenas;
    private ArenaSign[] join_signs;
    private ArenaSign[] quit_signs;
    private ArenaSign[] kit_signs;
    
    public TeamDeathmatchConfig(Game game, Messages messages, JsonLocation spawn, LoadoutInfo[] loadouts,
            ArenaConfig[] arenas, ArenaSign[] join, ArenaSign[] quit, ArenaSign[] kit) {
        this.game = game == null ? new Game() : game;
        this.messages = messages == null ? new Messages() : messages;
        this.loadouts = loadouts == null ? new LoadoutInfo[0] : loadouts;
        this.arenas = arenas == null ? new ArenaConfig[0] : arenas;
        this.join_signs = join == null ? new ArenaSign[0] : join;
        this.quit_signs = quit == null ? new ArenaSign[0] : quit;
        this.kit_signs = kit == null ? new ArenaSign[0] : kit;
    }
    
    public TeamDeathmatchConfig() {
        this(null, null, null, null, null, null, null, null);
    }
    
    public Game getGame() {
        return game;
    }
    
    public Messages getMessages() {
        return messages;
    }
    
    public LoadoutInfo[] getFallbackLoadouts() {
        return loadouts;
    }
    
    public ArenaConfig[] getArenas() {
        return arenas;
    }
    
    public ArenaSign[] getJoinSigns() {
        return join_signs;
    }
    
    public ArenaSign[] getQuitSigns() {
        return quit_signs;
    }
    
    public ArenaSign[] getKitSigns() {
        return kit_signs;
    }
    
    public class Game {
    
        private String name;
        private String spawnworld;
        private JsonLocation server_spawn;
        private int blockrate;
        private boolean superperms_override;
        
        public Game(String name, String spawnworld, JsonLocation spawn, int blockrate, boolean superperms) {
            this.name = name == null ? DEFAULT_GAMENAME : name;
            this.spawnworld = spawnworld == null ? "" : spawnworld;
            this.server_spawn = spawn == null ? new JsonLocation() : spawn;
            this.blockrate = blockrate;
            this.superperms_override = superperms;
        }
        
        public Game() {
            this(null, null, null, 1000, false);
        }
        
        public String getName() {
            return name == null ? name = DEFAULT_GAMENAME : name;
        }
        
        public String getSpawnWorld() {
            return spawnworld == null ? "" : spawnworld;
        }
        
        public JsonLocation getServerSpawn() {
            return server_spawn;
        }
        
        public int getBlockRate() {
            return blockrate;
        }
        
        public boolean shouldUseSuperPerms() {
            return superperms_override;
        }
    }
    
    public class Messages {
    
        private static final String DEFAULT_TEAMWIN = "The &6%s&r team won with %s kills!";
        private static final String DEFAULT_AWARD = "You have been awarded &6%s&r tokens!";
        private static final String DEFAULT_WITHHELD = "You have not been playing long enough to recieve tokens!";
        private static final String DEFAULT_WORLDGEN = "That arena is not currently open!";
        private static final String DEFAULT_SOFTFULL = "That arena is currently too full!";
        private static final String DEFAULT_HARDFULL = "That arena is currently too full!";
        private static final String DEFAULT_NOPERM = "You don't have permission to join that arena!";
        
        private String teamwin, awardtokens, withheldtokens, join_worldgen, join_hardfull, join_softfull, join_sign_noperm;
        
        public Messages(String teamwin, String award, String withheld, String worldgen, String hard, String soft, String noperm) {
            this.teamwin = teamwin == null ? DEFAULT_TEAMWIN : teamwin;
            this.awardtokens = award == null ? DEFAULT_AWARD : award;
            this.withheldtokens = withheld == null ? DEFAULT_WITHHELD : withheld;
            this.join_worldgen = worldgen == null ? DEFAULT_WORLDGEN : worldgen;
            this.join_softfull = soft == null ? DEFAULT_SOFTFULL : soft;
            this.join_hardfull = hard == null ? DEFAULT_HARDFULL : hard;
            this.join_sign_noperm = noperm == null ? DEFAULT_NOPERM : noperm;
        }
        
        public Messages() {
            this(null, null, null, null, null, null, null);
        }
        
        public String formatTeamWin(String team, int points) {
            if (teamwin == null) {
                this.teamwin = DEFAULT_TEAMWIN;
            }
            
            return ChatColor.translateAlternateColorCodes('&', String.format(teamwin, team, points));
        }
        
        public String formatAward(double amount) {
            if (awardtokens == null) {
                this.awardtokens = DEFAULT_AWARD;
            }
            
            return ChatColor.translateAlternateColorCodes('&', String.format(awardtokens, amount));
        }
        
        public String formatWithheld() {
            if (withheldtokens == null) {
                this.withheldtokens = DEFAULT_WITHHELD;
            }
            
            return ChatColor.translateAlternateColorCodes('&', withheldtokens);
        }
        
        public String formatJoinFailWorldgen() {
            if (join_worldgen == null) {
                this.join_worldgen = DEFAULT_WORLDGEN;
            }
            
            return ChatColor.translateAlternateColorCodes('&', join_worldgen);
        }
        
        public String formatJoinSoftFull() {
            if (join_softfull == null) {
                this.join_softfull = DEFAULT_SOFTFULL;
            }
            
            return ChatColor.translateAlternateColorCodes('&', join_softfull);
        }
        
        public String formatJoinHardFull() {
            if (join_hardfull == null) {
                this.join_hardfull = DEFAULT_HARDFULL;
            }
            
            return ChatColor.translateAlternateColorCodes('&', join_hardfull);
        }
        
        public String formatJoinNoPerm() {
            if (join_sign_noperm == null) {
                this.join_sign_noperm = DEFAULT_NOPERM;
            }
            
            return ChatColor.translateAlternateColorCodes('&', join_sign_noperm);
        }
    }
    
    
    public static boolean addChest(TeamDeathmatchConfig config, String arenalabel, ArenaChest chest) {
        ArenaConfig[] configs = config.getArenas();
        ArenaConfig chosen = null;
        
        for (ArenaConfig arena : configs) {
            if (Objects.equals(arenalabel, arena.getLabel())) {
                chosen = arena;
                break;
            }
        }
        
        if (chosen == null) {
            return false;
        }
        
        ArenaConfig.addChest(chosen, chest);
        config.save(TeamDeathmatch.getInstance().getConfigurationFile());
        return true;
    }
    
    public static void addJoinSign(TeamDeathmatchConfig config, ArenaSign sign) {
        config.join_signs = ArrayUtil.addElement(config.join_signs, sign);
        config.save(TeamDeathmatch.getInstance().getConfigurationFile());
    }
    
    public static void addQuitSign(TeamDeathmatchConfig config, ArenaSign sign) {
        config.quit_signs = ArrayUtil.addElement(config.quit_signs, sign);
        config.save(TeamDeathmatch.getInstance().getConfigurationFile());
    }
    
    public static void addKitSign(TeamDeathmatchConfig config, ArenaSign sign) {
        config.kit_signs = ArrayUtil.addElement(config.kit_signs, sign);
        config.save(TeamDeathmatch.getInstance().getConfigurationFile());
    }
    
    public static boolean addSpawn(TeamDeathmatchConfig config, String arenalabel, ArenaConfig.TeamSpawn spawn) {
        ArenaConfig[] configs = config.getArenas();
        ArenaConfig chosen = null;
        
        for (ArenaConfig arena : configs) {
            if (Objects.equals(arenalabel, arena.getLabel())) {
                chosen = arena;
                break;
            }
        }
        
        if (chosen == null) {
            return false;
        }
        
        ArenaConfig.addSpawn(chosen, spawn);
        config.save(TeamDeathmatch.getInstance().getConfigurationFile());
        return true;
    }
}
