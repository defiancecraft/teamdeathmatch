package io.github.gjosiah.tdm.util;

import java.util.function.Consumer;

import com.archeinteractive.defiancetools.util.ui.MenuItem;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class SimpleMenuItem extends MenuItem {

    private final Consumer<? super Player> click;
    
    public SimpleMenuItem(ItemStack item, Consumer<? super Player> click) {
        super(item.getItemMeta().getDisplayName(), item.getData());
        
        this.click = click;
    }
    
    public void onClick(Player player) {
        if (click != null) {
            click.accept(player);
        }
    }
}
