package io.github.gjosiah.tdm.config;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class LoadoutInfo {
    
    private JsonInventory.JsonInventorySlot icon;
    private String permission;
    private String file;
    private String message;
    private String noperm;
    
    public LoadoutInfo(JsonInventory.JsonInventorySlot icon, String permission, String filename, String message, String noperm) {
        this.icon = icon;
        this.permission = permission == null ? "" : permission;
        this.file = filename == null ? "" : filename;
        this.message = message == null ? "" : message;
        this.noperm = noperm == null ? "" : noperm;
    }
    
    public LoadoutInfo(ItemStack icon, String permission, String filename, String message, String noperm) {
        this(new JsonInventory.JsonInventorySlot(0, icon), permission, filename, message, noperm);
    }
    
    public LoadoutInfo() {
        this((JsonInventory.JsonInventorySlot) null, null, null, null, null);
    }
    
    public ItemStack getIcon() {
        return icon == null ? new ItemStack(Material.STONE) : icon.getItem();
    }
    
    public String getPermission() {
        return permission == null ? "" : permission;
    }
    
    public String getFile() {
        return file == null ? "" : file;
    }
    
    public String getEquipMessage() {
        return message == null ? "" : message;
    }
    
    public String getPermMessage() {
        return noperm == null ? "" : noperm;
    }
}
