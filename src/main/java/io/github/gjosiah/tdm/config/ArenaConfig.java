package io.github.gjosiah.tdm.config;

import io.github.gjosiah.tdm.TeamDeathmatch;
import io.github.gjosiah.tdm.util.ArrayUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;

public class ArenaConfig {

    private ArenaChest[] chests;
    private TeamSpawn[] spawns;
    private LoadoutInfo[] loadouts;
    private String[] schematics;
    private String label, world, exceedpermission;
    private JsonLocation origin, lobby;
    private int playerlimit, exceedlimit, minutes;
    private double mintime, winpoints, losepoints;
    
    public ArenaConfig(ArenaChest[] chests, TeamSpawn[] spawns, LoadoutInfo[] loadouts, String label, String world, int players, String eperm,
            int eplayers, int minutes, String[] schematicfiles, JsonLocation origin, JsonLocation lobby, double mintime, double winpoints, double losepoints) {
        this.chests = chests == null ? new ArenaChest[0] : chests;
        this.spawns = spawns == null ? new TeamSpawn[0] : spawns;
        this.loadouts = loadouts == null ? new LoadoutInfo[0] : loadouts;
        this.label = label == null ? "" : label;
        this.world = world == null ? "" : world;
        this.playerlimit = players;
        this.exceedpermission = eperm == null ? "" : eperm;
        this.exceedlimit = eplayers;
        this.minutes = minutes;
        this.schematics = schematicfiles == null ? new String[0] : schematicfiles;
        this.origin = origin == null ? new JsonLocation() : origin;
        this.lobby = lobby == null ? new JsonLocation() : lobby;
        this.mintime = mintime;
        this.winpoints = winpoints;
        this.losepoints = losepoints;
    }
    
    public ArenaConfig() {
        this(null, null, null, null, null, -1, null, -1, -1, null, null, null, 0d, 0d, 0d);
    }
    
    public ArenaChest[] getChests() {
        return chests;
    }
    
    public TeamSpawn[] getSpawns() {
        return spawns;
    }
    
    public LoadoutInfo[] getLoadouts() {
        return loadouts;
    }
    
    public JsonLocation[] getRedSpawns() {
        List<JsonLocation> red = new ArrayList<>();
        
        for (TeamSpawn spawn : spawns) {
            if ("RED".equalsIgnoreCase(spawn.team)) {
                red.add(spawn);
            }
        }
        
        return red.toArray(new JsonLocation[red.size()]);
    }
    
    public JsonLocation[] getBlueSpawns() {
        List<JsonLocation> blue = new ArrayList<>();
        
        for (TeamSpawn spawn : spawns) {
            if ("BLUE".equalsIgnoreCase(spawn.team)) {
                blue.add(spawn);
            }
        }
        
        return blue.toArray(new JsonLocation[blue.size()]);
    }
    
    public String getLabel() {
        return label;
    }
    
    public String getWorld() {
        return world;
    }
    
    public int getMaxPlayers() {
        return playerlimit;
    }
    
    public String getExceedPermission() {
        return exceedpermission;
    }
    
    public int getExceedMax() {
        return exceedlimit;
    }
    
    public int getMatchMinutes() {
        return minutes;
    }
    
    public String[] getSchematicFiles() {
        return schematics;
    }
    
    public JsonLocation getOrigin() {
        return origin;
    }
    
    public JsonLocation getLobbySpawn() {
        return lobby;
    }
    
    public double getMinimumPlayTime() {
        return mintime;
    }
    
    public double getWinningPoints() {
        return winpoints;
    }
    
    public double getLosingPoints() {
        return losepoints;
    }
    
    
    public static class TeamSpawn extends JsonLocation {
    
        private String team;
        
        public TeamSpawn(String team, Location loc) {
            this(team, loc.getX(), loc.getY(), loc.getX(), loc.getPitch(), loc.getYaw());
        }
        
        public TeamSpawn(String team, double x, double y, double z, float pitch, float yaw) {
            super(x, y, z, pitch, yaw);
            
            this.team = team == null ? "" : team;
        }
        
        public TeamSpawn() {
            super();
            
            this.team = "";
        }
        
        public String getTeam() {
            return team;
        }
    }
    
    static void addChest(ArenaConfig config, ArenaChest chest) {
        config.chests = ArrayUtil.addElement(config.chests, chest);
    }
    
    static void addSpawn(ArenaConfig config, TeamSpawn spawn) {
        config.spawns = ArrayUtil.addElement(config.spawns, spawn);
    }
    
    public static void addLoadout(TeamDeathmatchConfig mainconfig, ArenaConfig config, LoadoutInfo loadout) {
        config.loadouts = ArrayUtil.addElement(config.loadouts, loadout);
        mainconfig.save(TeamDeathmatch.getInstance().getConfigurationFile());
    }
    
    public static List<File> wrap(String[] filepaths, File root) {
        List<File> files = new ArrayList<>();
        
        for (String filepath : filepaths) {
            files.add(new File(root, filepath));
        }
        
        return files;
    }
}
