package io.github.gjosiah.tdm.config;

import org.bukkit.Location;

public class ArenaChest extends JsonLocation {

    private String contents;
    
    public ArenaChest(String file, Location loc) {
        super(loc.getX(), loc.getY(), loc.getZ(), loc.getPitch(), loc.getYaw());
        
        this.contents = file;
    }
    
    public ArenaChest() {
        this.contents = null;
    }
    
    public String getContentsFile() {
        return contents;
    }
}
