package io.github.gjosiah.tdm.impl;

import io.github.gjosiah.tdm.TeamDeathmatch;
import io.github.gjosiah.tdm.config.TeamDeathmatchConfig;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Called by an arena after an update has occurred.
 */
public class ArenaUpdateEvent extends Event {

    private static final HandlerList handlers = new HandlerList();
    
    private final Arena arena;
    
    public ArenaUpdateEvent(Arena arena) {
        this.arena = arena;
    }
    
    public Arena getArena() {
        return arena;
    }
    
    public TeamDeathmatchConfig getPluginConfig() {
        return TeamDeathmatch.getInstance().getConfiguration();
    }
    
    public HandlerList getHandlers() {
        return handlers;
    }
    
    
    public static HandlerList getHandlerList() { 
        return handlers;
    }
}
