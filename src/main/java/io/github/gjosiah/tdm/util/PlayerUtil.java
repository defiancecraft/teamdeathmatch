package io.github.gjosiah.tdm.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import com.archeinteractive.defiancetools.util.reflection.CommonReflection;
import com.archeinteractive.defiancetools.util.reflection.VersionHandler;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class PlayerUtil {

    private static final Class<?> classCraftPlayer = VersionHandler.getOBCClass("entity.CraftPlayer");
    private static final Method CraftPlayer_getHandle = CommonReflection.getMethod(classCraftPlayer, "getHandle", 0);
    
    private static final Class<?> classEntityPlayer = VersionHandler.getNMSClass("EntityPlayer");
    private static final Field EntityPlayer_playerConnection = CommonReflection.getField(classEntityPlayer, "playerConnection");
    
    private static final Class<?> classPacket = VersionHandler.getNMSClass("PacketPlayInClientCommand");
    private static final Class<?> enumClientCommand = VersionHandler.getNMSClass("EnumClientCommand");
    private static final Field PERFORM_RESPAWN = CommonReflection.getField(enumClientCommand, "PERFORM_RESPAWN");
    private static final Method packetMethod = CommonReflection.getMethod(
            EntityPlayer_playerConnection.getType(), "a", new Class<?>[] { classPacket });
    
    public static void respawn(Player player, Location spawn) {
        Location previous = player.getBedSpawnLocation();
        
        player.setBedSpawnLocation(spawn, true);
        respawn(player);
        player.teleport(spawn);
        
        // Reset their bed spawn
        player.setBedSpawnLocation(previous, true);
    }
    
    public static void respawn(Player player) {
        try {
            Object packet = classPacket.getConstructor(enumClientCommand).newInstance(PERFORM_RESPAWN.get(null));
            Object nmsplayer = CraftPlayer_getHandle.invoke(player);
            packetMethod.invoke(EntityPlayer_playerConnection.get(nmsplayer), packet);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
