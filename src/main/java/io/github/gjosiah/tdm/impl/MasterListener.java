package io.github.gjosiah.tdm.impl;

import io.github.gjosiah.tdm.TeamDeathmatch;
import io.github.gjosiah.tdm.config.ArenaSign;
import io.github.gjosiah.tdm.config.TeamDeathmatchConfig;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.google.common.collect.Iterators;

public class MasterListener implements Listener {

    private static final String PLAYERCT_FORMAT = ChatColor.DARK_PURPLE + "[%s/%s]";
    
    private final TeamDeathmatch instance;
    private final Set<ArenaSign> joinsigns, quitsigns, kitsigns;
    
    public MasterListener(TeamDeathmatch instance) {
        TeamDeathmatchConfig config = instance.getConfiguration();
        
        this.instance = instance;
        this.joinsigns = new HashSet<>();
        this.quitsigns = new HashSet<>();
        this.kitsigns = new HashSet<>();
        
        Iterators.forArray(config.getJoinSigns()).forEachRemaining(joinsigns::add);
        Iterators.forArray(config.getQuitSigns()).forEachRemaining(quitsigns::add);
        Iterators.forArray(config.getKitSigns()).forEachRemaining(kitsigns::add);
    }
    
    public Set<ArenaSign> getJoinSigns() {
        return Collections.unmodifiableSet(joinsigns);
    }
    
    public Set<ArenaSign> getQuitSigns() {
        return Collections.unmodifiableSet(quitsigns);
    }
    
    public Set<ArenaSign> getKitSigns() {
        return Collections.unmodifiableSet(kitsigns);
    }
    
    @EventHandler
    public void onArenaUpdate(ArenaUpdateEvent e) {
        for (ArenaSign sign : joinsigns) {
            if (Objects.equals(e.getArena().getLabel(), sign.getArena())) {
                World world = Bukkit.getWorld(sign.getWorldName());
                
                if (world != null) {
                    Block block = world.getBlockAt(sign.getX(), sign.getY(), sign.getZ());
                    BlockState state = block.getState();
                    
                    if (state instanceof Sign) {
                        Sign s = (Sign) state;
                        boolean softfull = e.getArena().getPlayerCount() >= e.getArena().getSoftMax();
                        boolean absfull = e.getArena().getPlayerCount() >= e.getArena().getMaxPlayers();
                        s.setLine(0, ChatColor.DARK_BLUE + e.getArena().getLabel());
                        
                        s.setLine(2, String.format(PLAYERCT_FORMAT, e.getArena().getPlayerCount(), e.getArena().getMaxPlayers()));
                        
                        String description = GamePhase.INPROGRESS.equals(e.getArena().getPhase()) ?
                                softfull ? absfull ? "full" : "overflow" : "open" : "restoring";
                        s.setLine(1, ChatColor.DARK_BLUE + description);
                        
                        s.update(true, false);
                    }
                }
            }
        }
    }
    
    @EventHandler
    public void onSignClick(PlayerInteractEvent e) {
        Block clicked = e.getClickedBlock();
        
        if (clicked == null || !(clicked.getState() instanceof Sign)) {
            return;
        }
        
        Location loc = clicked.getLocation();
        Player player = e.getPlayer();
        
        for (ArenaSign kit : kitsigns) {
            if (kit.representsLocation(loc)) {
                Arena arena = TeamDeathmatch.getInstance().getArenas().getArena(kit.getArena());
                
                if (arena != null) {
                    arena.getLoadoutMenu().openMenu(player);
                    return;
                }
            }
        }
        
        for (ArenaSign join : joinsigns) {
            if (join.representsLocation(loc)) {
                if (TeamDeathmatch.getInstance().hasPermission(player, join.getPermission())) {
                    instance.getArenas().sendToArena(join, e.getPlayer());
                } else {
                    player.sendMessage(instance.getConfiguration().getMessages().formatJoinNoPerm());
                }
                
                return;
            }
        }
        
        for (ArenaSign quit : quitsigns) {
            if (quit.representsLocation(loc) && TeamDeathmatch.getInstance().hasPermission(player, quit.getPermission())) {
                instance.getArenas().removeFromArena(e.getPlayer());
                return;
            }
        }
    }
    
    @EventHandler
    public void onInventoryClose(InventoryCloseEvent e) {
        if (e.getPlayer() instanceof Player) {
            if (e.getInventory() != null && e.getInventory().getHolder() instanceof LoadoutMenu) {
                LoadoutMenu menu = (LoadoutMenu) e.getInventory().getHolder();
                menu.checkDefault((Player) e.getPlayer());
            }
        }
    }
    
    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        String quitworld = e.getPlayer().getWorld().getName();
        boolean dospawn = false;
        
        for (Map.Entry<String, Arena> entry : TeamDeathmatch.getInstance().getArenas()) {
            if (quitworld.equals(entry.getValue().getWorldName())) {
                dospawn = true;
                break;
            }
        }
        
        if (!dospawn) {
            return;
        }
        
        TeamDeathmatchConfig config = TeamDeathmatch.getInstance().getConfiguration();
        Location spawn = config.getGame().getServerSpawn().toLocation(Bukkit.getWorld(config.getGame().getSpawnWorld()));
        
        if (spawn != null) {
            e.getPlayer().teleport(spawn);
        }
    }
}
