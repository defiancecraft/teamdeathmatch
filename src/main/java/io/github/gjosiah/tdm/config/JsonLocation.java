package io.github.gjosiah.tdm.config;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.util.Vector;

public class JsonLocation {

    private double x, y, z;
    private float pitch, yaw;
    
    public JsonLocation(double x, double y, double z, float pitch, float yaw) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.pitch = pitch;
        this.yaw = yaw;
    }
    
    public JsonLocation() {
        this(0d, 0d, 0d, 0f, 0f);
    }
    
    public Location toLocation(World world) {
        return world == null ? null : new Location(world, x, y, z, yaw, pitch);
    }
    
    public Vector toVector() {
        return new Vector(x, y, z);
    }
    
    public double getX() {
        return x;
    }
    
    public double getY() {
        return y;
    }
    
    public double getZ() {
        return z;
    }
    
    public float getPitch() {
        return pitch;
    }
    
    public float getYaw() {
        return yaw;
    }
}
