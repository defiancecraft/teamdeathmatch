package io.github.gjosiah.tdm.util;

import io.github.gjosiah.tdm.TeamDeathmatch;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.scheduler.BukkitRunnable;

import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.blocks.BaseBlock;
import com.sk89q.worldedit.schematic.SchematicFormat;

/**
 * A BukkitRunnable extension for delegating the pasting of the blocks of a
 * schematic over multiple server ticks by limiting the number of blocks that
 * can be pasted per call to the {@link #run()} method (defaults to 100 / tick).
 */
@SuppressWarnings("deprecation")
public class SchematicPasteTask extends BukkitRunnable {

    private static final int DEFAULT_ALLOWED = 1000;
    
    final int xi, yi, zi, xo, yo, zo, allowed;
    private volatile List<Runnable> callbacks;
    private volatile World world;
    private volatile CuboidClipboard schematic;
    private volatile int i, j, k, changed;
    
    public SchematicPasteTask(Location initial, File schematic) {
        this(initial, schematic, DEFAULT_ALLOWED);
    }
    
    public SchematicPasteTask(World world, int xi, int yi, int zi, File schematic) {
        this(world, xi, yi, zi, schematic, DEFAULT_ALLOWED);
    }
    
    public SchematicPasteTask(Location initial, File schematic, int pertick) {
        this(initial.getWorld(), initial.getBlockX(), initial.getBlockY(), initial.getBlockZ(), schematic, pertick);
    }
    
    public SchematicPasteTask(World world, int xi, int yi, int zi, File schematic, int pertick) {
        this(world, xi, yi, zi, load(schematic), pertick);
    }
    
    private SchematicPasteTask(World world, int xi, int yi, int zi, CuboidClipboard schematic, int pertick) {
        this.xi = xi;
        this.yi = yi;
        this.zi = zi;
        this.world = world;
        this.schematic = schematic;
        this.allowed = pertick;
        this.callbacks = new ArrayList<>();
        
        Vector size = schematic.getSize();
        this.xo = size.getBlockX();
        this.yo = size.getBlockY();
        this.zo = size.getBlockZ();
        this.i = 0;
        this.j = 0;
        this.k = 0;
    }
    
    public SchematicPasteTask callback(Runnable r) {
        if (callbacks == null) {
            throw new IllegalStateException("Cannot accept callbacks as this SchematicPasteTask has already finished!");
        }
        
        callbacks.add(r);
        return this;
    }
    
    public void run() {
        if (world == null || schematic == null) {
            throw new IllegalStateException("This SchematicPasteTask has insufficient information to paste!");
        }
        
        this.changed = 0;
        
        for (; i < xo; i()) {
            for (; j < yo; j()) {
                for (; k < zo; k()) {
                    if (changed++ > allowed) {
                        return;
                    }
                    
                    BaseBlock block = schematic.getBlock(new Vector(i, j, k));
                    Block b = world.getBlockAt(xi + i, yi + j, zi + k);
                    b.setTypeId(block.getId());
                    b.setData((byte) block.getData());
                    
                    BlockState state;
                    
                    if ((state = b.getState()) instanceof InventoryHolder) {
                        ((InventoryHolder) state).getInventory().clear();
                    }
                }
            }
        }
        
        cancel();
    }
    
    private void i() {
        this.i++;
        this.j = 0;
        this.k = 0;
    }
    
    private void j() {
        this.j++;
        this.k = 0;
    }
    
    private void k() {
        this.k++;
    }
    
    public void cancel() {
        callbacks.forEach(r -> {
            try {
                r.run();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        
        try {
            String info = String.format("Finished pasting schematic at %s[%s, %s, %s]", world.getName(), xi, yi, zi);
            TeamDeathmatch.getInstance().getLogger().info(info);
            
            super.cancel();
            this.callbacks = null;
            this.world = null;
            this.schematic = null;
        } catch (Exception e) {
            // Task was not running after all
        }
    }
    
    private static CuboidClipboard load(File schematic) {
        try {
            return SchematicFormat.MCEDIT.load(schematic);
        } catch (Exception e) {
            throw new IllegalArgumentException("Could not load a schematic from the given File!", e);
        }
    }
}
