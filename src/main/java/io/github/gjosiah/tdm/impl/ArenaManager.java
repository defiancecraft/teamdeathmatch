package io.github.gjosiah.tdm.impl;

import io.github.gjosiah.tdm.TeamDeathmatch;
import io.github.gjosiah.tdm.config.ArenaConfig;
import io.github.gjosiah.tdm.config.ArenaSign;
import io.github.gjosiah.tdm.config.LoadoutInfo;
import io.github.gjosiah.tdm.config.TeamDeathmatchConfig;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class ArenaManager implements Iterable<Map.Entry<String, Arena>> {

    private final TeamDeathmatch instance;
    private final HashMap<String, Arena> arenas;
    private final TeamDeathmatchConfig config;
    private final Loadout[] loadouts;
    
    public ArenaManager(TeamDeathmatch instance, TeamDeathmatchConfig config) {
        this.instance = instance;
        this.arenas = new HashMap<>();
        this.config = config;
        this.loadouts = new Loadout[config.getFallbackLoadouts().length];
        
        for (int i = 0; i < loadouts.length; i++) {
            LoadoutInfo loadout = config.getFallbackLoadouts()[i];
            
            if (loadout != null) {
                loadouts[i] = new Loadout(loadout);
            }
        }
    }
    
    public void dispose() {
        for (Arena arena : arenas.values()) {
            arena.removeAllPlayers();
        }
    }
    
    public boolean hasArena(String label) {
        for (Arena arena : arenas.values()) {
            if (Objects.equals(label, arena.getLabel())) {
                return true;
            }
        }
        
        return false;
    }
    
    Loadout[] getFallbackLoadouts() {
        if (loadouts == null) {
            return new Loadout[0];
        } else {
            return loadouts;
        }
    }
    
    public Iterator<Map.Entry<String, Arena>> iterator() {
        return arenas.entrySet().iterator();
    }
    
    public boolean sendToArena(String label, Player player) {
        for (Arena arena : arenas.values()) {
            if (arena.hasPlayer(player)) {
                return false;
            }
        }
        
        if (arenas.containsKey(label)) {
            Arena arena = arenas.get(label);
            return arena.addPlayer(player);
        } else {
            return false;
        }
    }
    
    public boolean sendToArena(ArenaSign arena, Player player) {
        return sendToArena(arena.getArena(), player);
    }
    
    public boolean removeFromArena(Player player) {
        boolean found = false;
        
        for (Arena arena : arenas.values()) {
            if (arena.removePlayer(player)) {
                found = true;
                break;
            }
        }
        
        if (!found) {
            return false;
        }
        
    	Location spawn = config.getGame().getServerSpawn().toLocation(Bukkit.getWorld(config.getGame().getSpawnWorld()));
    	
    	if (spawn != null) {
    		player.teleport(spawn);
    	}
    	
    	return true;
    }
    
    public void initArena(ArenaConfig config) {
        try {
            Arena arena = new Arena(config, this);
            arenas.put(config.getLabel(), arena);
            
            Bukkit.getPluginManager().registerEvents(arena, instance);
            instance.getLogger().info("Created arena " + config.getLabel() + " in world " + config.getWorld() + ".");
        } catch (IllegalArgumentException e) {
            instance.getLogger().warning("Could not create arena " + config.getLabel() + ", likely because its schematic doesn't exist.");
            e.printStackTrace();
        } catch (Exception e) {
            instance.getLogger().warning("Could not create arena " + config.getLabel() + "!");
            e.printStackTrace();
        }
    }

    public Arena getArena(Player player) {
        for (Arena arena : arenas.values()) {
            if (arena.hasPlayer(player)) {
                return arena;
            }
        }
        
        return null;
    }
    
    public Arena getArena(String label) {
        for (Arena arena : arenas.values()) {
            if (arena.getLabel().equals(label)) {
                return arena;
            }
        }
        
        return null;
    }
    
    public ArenaConfig getArenaConfig(Arena arena) {
        if (arena == null) {
            return null;
        }
        
        if (!arenas.values().contains(arena)) {
            return null;
        }
        
        for (ArenaConfig aconfig : config.getArenas()) {
            if (arena.getLabel().equals(aconfig.getLabel())) {
                return aconfig;
            }
        }
        
        return null;
    }
    
    public ArenaConfig getArenaConfig(String label) {
        if (label == null) {
            return null;
        }
        
        for (ArenaConfig aconfig : config.getArenas()) {
            if (label.equals(aconfig.getLabel())) {
                return aconfig;
            }
        }
        
        return null;
    }
    
    
    public static Loadout getLoadout(Loadout[] loadouts, Player player) {
        TeamDeathmatch instance = TeamDeathmatch.getInstance();
        
        if (loadouts.length < 1 || loadouts[0] == null) {
            return null;
        } else {
            if (instance.hasPermission(player, loadouts[0].permission)) {
                return loadouts[0];
            } else {
                return null;
            }
        }
        
//        int index = -1;
//        
//        for (int i = 0; i < loadouts.length; i++) {
//            if (instance.hasPermission(player, loadouts[i].permission)) {
//                index = i;
//                continue;
//            } else {
//                break;
//            }
//        }
//        
//        if (index < 0) {
//            return null;
//        } else {
//            return loadouts[index];
//        }
    }
}
