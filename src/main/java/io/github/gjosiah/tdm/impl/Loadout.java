package io.github.gjosiah.tdm.impl;

import io.github.gjosiah.tdm.TeamDeathmatch;
import io.github.gjosiah.tdm.config.JsonInventory;
import io.github.gjosiah.tdm.config.LoadoutInfo;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Loadout {

    final String permission, equip_message, perm_message;
    final ItemStack icon;
    final Map<Integer, ItemStack> items;
    
    Loadout(LoadoutInfo info) {
        ItemStack icon = info.getIcon();
        this.icon = icon == null ? new ItemStack(Material.STONE) : icon;
        this.items = new HashMap<>();
        this.permission = info.getPermission();
        this.equip_message = info.getEquipMessage();
        this.perm_message = info.getPermMessage();
        
        File file = new File(TeamDeathmatch.getInstance().getDataFolder(), info.getFile());
        
        if (!file.isFile()) {
            throw new IllegalArgumentException("The given loadout's file doesn't exist!");
        }
        
        try (FileReader reader = new FileReader(file)) {
            JsonInventory inv = TeamDeathmatch.GSON.fromJson(reader, JsonInventory.class);
            
            for (JsonInventory.JsonInventorySlot slot : inv) {
                items.put(slot.getIndex(), slot.getItem());
            }
        } catch (IOException e) {
            throw new IllegalArgumentException("The given loadout's file could not be read!");
        }
    }
    
    public void checkEquip(Player player) {
        if (TeamDeathmatch.getInstance().hasPermission(player, permission)) {
            equip(player, true);
        } else {
            player.sendMessage(perm_message);
        }
    }
    
    public void equip(Player player, boolean initial) {
        for (Map.Entry<Integer, ItemStack> item : items.entrySet()) {
            player.getInventory().setItem(item.getKey(), item.getValue());
        }
        
        player.updateInventory();
        
        if (initial && equip_message != null && !equip_message.isEmpty()) {
            player.sendMessage(equip_message);
        }
    }
    
    public void equip(Player player) {
        equip(player, false);
    }
}
