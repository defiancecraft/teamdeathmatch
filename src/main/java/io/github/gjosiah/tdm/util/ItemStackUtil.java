package io.github.gjosiah.tdm.util;

import java.util.Arrays;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemStackUtil {

    private ItemStackUtil() {}
    
    
    public static ItemStack modify(ItemStack item, String display, String[] lore) {
        ItemMeta meta = item.getItemMeta();
        
        if (display != null) {
            meta.setDisplayName(display);
        }
        
        if (lore != null && lore.length > 0) {
            meta.setLore(Arrays.asList(lore));
        }
        
        item.setItemMeta(meta);
        return item;
    }
}
