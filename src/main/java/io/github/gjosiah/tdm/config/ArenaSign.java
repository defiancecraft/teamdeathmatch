package io.github.gjosiah.tdm.config;

import java.util.Objects;

import org.bukkit.Location;

public class ArenaSign extends JsonWorldBlock {

    private String arena;
    private String permission;
    
    public ArenaSign(String arena, String permission, String world, int x, int y, int z) {
        super(world, x, y, z);
        
        this.arena = arena;
        this.permission = permission;
    }
    
    public ArenaSign() {
        super();
        
        this.arena = "";
        this.permission = "";
    }
    
    public String getArena() {
        return arena == null ? arena = "" : arena;
    }
    
    public String getPermission() {
        return permission == null ? permission = "" : permission;
    }
    
    public boolean representsLocation(Location location) {
        boolean world = Objects.equals(location.getWorld().getName(), getWorldName());
        boolean x = location.getBlockX() == getX();
        boolean y = location.getBlockY() == getY();
        boolean z = location.getBlockZ() == getZ();
        
        return world && x && y && z;
    }
}
