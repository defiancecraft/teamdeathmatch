package io.github.gjosiah.tdm.util;

import java.util.Arrays;

public class ArrayUtil {

    public static <T> T[] addElement(T[] array, T element) {
        T[] copy = Arrays.copyOfRange(array, 0, array.length + 1);
        copy[array.length] = element;
        return copy;
    }
}
