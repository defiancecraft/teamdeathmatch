package io.github.gjosiah.tdm.config;

import org.bukkit.World;
import org.bukkit.block.Block;

public class JsonWorldBlock {

    private String world;
    private int x, y, z;
    
    public JsonWorldBlock(String world, int x, int y, int z) {
        setWorldName(world);
        setX(x);
        setY(y);
        setZ(z);
    }
    
    public JsonWorldBlock() {
        this("", 0, 0, 0);
    }
    
    public String getWorldName() {
        return world == null ? world = "" : world;
    }
    
    public int getX() {
        return x;
    }
    
    public int getY() {
        return y;
    }
    
    public int getZ() {
        return z;
    }
    
    public void setWorldName(String worldname) {
        this.world = worldname == null ? "" : worldname;
    }
    
    public void setX(int x) {
        this.x = x;
    }
    
    public void setY(int y) {
        this.y = y;
    }
    
    public void setZ(int z) {
        this.z = z;
    }
    
    public Block toBlock(World world) {
        return world.getBlockAt(x, y, z);
    }
}
