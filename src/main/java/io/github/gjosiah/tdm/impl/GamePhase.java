package io.github.gjosiah.tdm.impl;

public enum GamePhase {

    INPROGRESS(true, true),
    WORLDGEN(false, false);
    
    private final boolean playing, accepting;
    
    private GamePhase(boolean playing, boolean accepting) {
        this.playing = playing;
        this.accepting = accepting;
    }
    
    public boolean isPlaying() {
        return playing;
    }
    
    public boolean isAcceptingPlayers() {
        return accepting;
    }
}
