package io.github.gjosiah.tdm.impl;

import io.github.gjosiah.tdm.TeamDeathmatch;
import io.github.gjosiah.tdm.config.ArenaChest;
import io.github.gjosiah.tdm.config.JsonInventory;
import io.github.gjosiah.tdm.config.JsonLocation;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class InitializedChest {

    private final JsonInventory inv;
    private final JsonLocation loc;
    
    InitializedChest(JsonInventory inv, JsonLocation loc) {
        this.inv = inv;
        this.loc = loc;
    }
    
    public JsonInventory getInventory() {
        return inv;
    }
    
    public JsonLocation getLocation() {
        return loc;
    }
    
    
    public static InitializedChest[] forArray(ArenaChest[] chests) {
        List<InitializedChest> initialized = new ArrayList<>();
        File dir = TeamDeathmatch.getInstance().getDataFolder();
        
        for (ArenaChest chest : chests) {
            File inventory = new File(dir, chest.getContentsFile());
            
            if (!inventory.isFile()) {
                TeamDeathmatch.getInstance().getLogger().warning(
                        "Cannot locate inventory file at " + inventory.getAbsolutePath() + " for chest, skipping.");
                continue;
            }
            
            try (FileReader reader = new FileReader(inventory)) {
                JsonInventory inv = TeamDeathmatch.GSON.fromJson(reader, JsonInventory.class);
                initialized.add(new InitializedChest(inv, chest));
            } catch(IOException e) {
                TeamDeathmatch.getInstance().getLogger().warning(
                        "Failed to read file at " + inventory.getAbsolutePath() + " for chest, skipping.");
                continue;
            }
        }
        
        return initialized.toArray(new InitializedChest[initialized.size()]);
    }
}
