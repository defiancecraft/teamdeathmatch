package io.github.gjosiah.tdm.impl;

import io.github.gjosiah.tdm.TeamDeathmatch;
import io.github.gjosiah.tdm.config.ArenaConfig;
import io.github.gjosiah.tdm.config.JsonLocation;
import io.github.gjosiah.tdm.config.LoadoutInfo;
import io.github.gjosiah.tdm.config.TeamDeathmatchConfig;
import io.github.gjosiah.tdm.util.ArenaInitializeTask;
import io.github.gjosiah.tdm.util.PlayerUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import java.util.function.Predicate;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

public class Arena implements Listener {

    private static final Random random = new Random();
    private static final String RED_POINTS = "Red Points";
    private static final String BLUE_POINTS = "Blue Points";
    private static final String TIME_REM = "Time Remaining";
    private static final int JOINING_SECONDS = 2;
    
    private final Set<GamePlayer> players;
    private final Set<String> respawning;
    private final Map<String, ItemStack[]> joining;
    private final String label, worldname, privilege;
    private final InitializedChest[] chests;
    private final JsonLocation[] redspawns, bluespawns;
    private final JsonLocation origin, lobby;
    private final List<File> schematics;
    private final Loadout[] loadouts;
    private final LoadoutMenu menu;
    private final Scoreboard scoreboard;
    private final Objective objective;
    private final int max, privileged, length;
    private final double minplaytime, win, lose;
    private final boolean lobbyenabled;
    private int redpoints, bluepoints, schematicindex;
    private GamePhase phase;
    
    public Arena(ArenaConfig config, ArenaManager amanager) {
        this.label = config.getLabel();
        this.worldname = config.getWorld();
        this.max = config.getMaxPlayers();
        this.players = new HashSet<>(max, 1f);
        this.respawning = new HashSet<>(max, 1f);
        this.joining = new HashMap<>(max, 1f);
        this.privilege = config.getExceedPermission();
        this.privileged = config.getExceedMax();
        this.length = config.getMatchMinutes();
        this.minplaytime = config.getMinimumPlayTime();
        this.win = config.getWinningPoints();
        this.lose = config.getLosingPoints();
        this.chests = InitializedChest.forArray(config.getChests());
        this.redspawns = config.getRedSpawns();
        this.bluespawns = config.getBlueSpawns();
        this.origin = config.getOrigin();
        this.lobby = config.getLobbySpawn();
        this.lobbyenabled = lobby != null;
        this.schematics = ArenaConfig.wrap(config.getSchematicFiles(), TeamDeathmatch.getInstance().getDataFolder());
        this.schematicindex = 0;
        this.scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        this.phase = GamePhase.WORLDGEN;
        
        Loadout[] temploadouts = new Loadout[config.getLoadouts().length];
        
        for (int i = 0; i < temploadouts.length; i++) {
            LoadoutInfo loadout = config.getLoadouts()[i];
            
            if (loadout != null) {
                temploadouts[i] = new Loadout(loadout);
            }
        }
        
        if (temploadouts != null && temploadouts.length > 0) {
            this.loadouts = temploadouts;
        } else {
            this.loadouts = amanager.getFallbackLoadouts();
            TeamDeathmatch.getInstance().getLogger().info("Using fallback loadouts for arena " + label + ".");
        }
        
        this.menu = new LoadoutMenu(this);
        
        schematics.removeIf(((Predicate<File>) File::isFile).negate());
        
        if (schematics.size() > 0) {
            TeamDeathmatch.getInstance().getLogger().info("Successfully created Arena '" + label + "'.");
        } else {
            throw new IllegalArgumentException("No schematic files were given and/or non existed!");
        }
        
        org.bukkit.scoreboard.Team redi = scoreboard.registerNewTeam("ind_red");
        org.bukkit.scoreboard.Team bluei = scoreboard.registerNewTeam("ind_blue");
        org.bukkit.scoreboard.Team goldi = scoreboard.registerNewTeam("ind_gold");
        redi.setPrefix(ChatColor.RED.toString());
        bluei.setPrefix(ChatColor.BLUE.toString());
        goldi.setPrefix(ChatColor.GOLD.toString());
        redi.addEntry(RED_POINTS);
        bluei.addEntry(BLUE_POINTS);
        goldi.addEntry(TIME_REM);
        
        this.objective = scoreboard.registerNewObjective("score", "dummy");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        objective.getScore(RED_POINTS).setScore(0);
        objective.getScore(BLUE_POINTS).setScore(0);
        objective.getScore(TIME_REM).setScore(length * 60);
        objective.setDisplayName(TeamDeathmatch.getInstance().getConfiguration().getGame().getName());
    }
    
    // Initialize the game, assuming it is in GamePhase.WORLDGEN- the task will automatically begin the game afterward.
    public void initialize() {
        if (!GamePhase.WORLDGEN.equals(phase)) {
            throw new IllegalStateException("Can only initialize during WORLDGEN!");
        }
        
        Bukkit.getPluginManager().callEvent(new ArenaUpdateEvent(this));
        new ArenaInitializeTask(this, this::begin).runTaskTimer(TeamDeathmatch.getInstance(), 1L, 60 * 20);
    }
    
    // Begin the game, sending the game into GamePhase.INPROGRESS.
    private void begin() {
        class Time {
        
            int remaining = length * 60;
            BukkitTask task = null;
        }
        
        Time time = new Time();
        objective.getScore(TIME_REM).setScore(time.remaining);
        
        this.phase = GamePhase.INPROGRESS;
        time.task = Bukkit.getScheduler().runTaskTimer(TeamDeathmatch.getInstance(), () -> {
            if (time.remaining-- > 0) {
                this.tick(time.remaining);
            } else {
                try {
                    time.task.cancel();
                    this.reset();
                } catch (Exception e) {
                    TeamDeathmatch.getInstance().getLogger().warning("Could not stop tick task for arena `" + label + "`!");
                }
            }
        }, 0L, 20);
        Bukkit.getPluginManager().callEvent(new ArenaUpdateEvent(this));
        
        for (Iterator<GamePlayer> it = players.iterator(); it.hasNext(); ) {
            GamePlayer gp = it.next();
            Player player = Bukkit.getPlayer(gp.name);
            
            if (player == null) {
                it.remove();
                continue;
            }
            
            equipPlayer(gp, player, true);
            
            if (gp.loadout == null) {
                menu.openMenu(player);
            }
        }
    }
    
    private void tick(int secs_rem) {
        objective.getScore(TIME_REM).setScore(secs_rem);
    }
    
    private void awardRed(GamePlayer gp) {
        Player p;
        
        if ((p = Bukkit.getPlayer(gp.name)) != null) {
            TeamDeathmatch tdm = TeamDeathmatch.getInstance();
            long played = System.currentTimeMillis() - gp.joined;
            boolean award = played >= (minplaytime * 60 * 1000);
            
            if (!award) {
                p.sendMessage(tdm.getConfiguration().getMessages().formatWithheld());
                return;
            }
            
            switch (gp.team) {
            case RED:
                p.sendMessage(tdm.getConfiguration().getMessages().formatAward(win));
                tdm.awardCurrency(p, win);
                return;
            case BLUE:
                p.sendMessage(tdm.getConfiguration().getMessages().formatAward(lose));
                tdm.awardCurrency(p, lose);
                return;
            }
        }
    }
    
    private void awardBlue(GamePlayer gp) {
        Player p;
        
        if ((p = Bukkit.getPlayer(gp.name)) != null) {
            TeamDeathmatch tdm = TeamDeathmatch.getInstance();
            long played = System.currentTimeMillis() - gp.joined;
            boolean award = played >= (minplaytime * 60 * 1000);
            
            if (!award) {
                p.sendMessage(tdm.getConfiguration().getMessages().formatWithheld());
                return;
            }
            
            switch (gp.team) {
            case BLUE:
                p.sendMessage(tdm.getConfiguration().getMessages().formatAward(win));
                tdm.awardCurrency(p, win);
                return;
            case RED:
                p.sendMessage(tdm.getConfiguration().getMessages().formatAward(lose));
                tdm.awardCurrency(p, lose);
                return;
            }
        }
    }
    
    private void broadcastWin(Team won, int points) {
        String message = TeamDeathmatch.getInstance().getConfiguration().getMessages().formatTeamWin(won.toString(), points);
        
        players.forEach(gp -> {
            Player p = Bukkit.getPlayer(gp.name);
            
            if (p != null) {
                p.sendMessage(message);
            }
        });
    }
    
    private void reset() {
        if (redpoints > bluepoints) {
            // Red won
            broadcastWin(Team.RED, redpoints);
            players.forEach(this::awardRed);
        } else if (bluepoints > redpoints) {
            // Blue won
            broadcastWin(Team.BLUE, bluepoints);
            players.forEach(this::awardBlue);
        } else {
            // "Tie"
            if (random.nextBoolean()) {
                objective.getScore(RED_POINTS).setScore(++redpoints);
                broadcastWin(Team.RED, redpoints);
                players.forEach(this::awardRed);
            } else {
                objective.getScore(BLUE_POINTS).setScore(++bluepoints);
                broadcastWin(Team.BLUE, bluepoints);
                players.forEach(this::awardBlue);
            }
        }
        
        this.redpoints = 0;
        this.bluepoints = 0;
        objective.getScore(RED_POINTS).setScore(0);
        objective.getScore(BLUE_POINTS).setScore(0);
        
        // If the lobby is enabled, don't remove players from the arena, use the lobby instead
        if (lobbyenabled) {
            for (GamePlayer gp : players) {
                Player player = Bukkit.getPlayer(gp.name);
                unequipPlayer(player);
                
                if (player != null) {
                    player.teleport(lobby.toLocation(getWorld()));
                }
            }
            
            this.phase = GamePhase.WORLDGEN;
            initialize();
            return;
        }
        
        // Otherwise, the lobby isn't enabled and we need to remove all the players to regenerate the world
        TeamDeathmatchConfig config = TeamDeathmatch.getInstance().getConfiguration();
        Location spawn = config.getGame().getServerSpawn().toLocation(Bukkit.getWorld(config.getGame().getSpawnWorld()));
        
        Set<GamePlayer> remove = new HashSet<>(players);
        players.clear();
        
        for (GamePlayer gp : remove) {
            Player player = Bukkit.getPlayer(gp.name);
            unequipPlayer(player);
            
            if (spawn != null && player != null) {
                player.teleport(spawn);
            }
        }
        
        remove.clear();
        this.phase = GamePhase.WORLDGEN;
        initialize();
        return;
    }
    
    public String getLabel() {
        return label;
    }
    
    public GamePhase getPhase() {
        return phase;
    }
    
    public String getWorldName() {
        return worldname;
    }
    
    public boolean isWorldLoaded() {
        return getWorld() != null;
    }
    
    public World getWorld() {
        return Bukkit.getWorld(worldname);
    }
    
    public int getPlayerCount() {
        return players.size();
    }
    
    public int getSoftMax() {
        return max;
    }
    
    public int getMaxPlayers() {
        return privileged;
    }
    
    public JsonLocation getOrigin() {
        return origin;
    }
    
    public File getSchematic() {
        if (schematicindex >= schematics.size()) {
            schematicindex = 0;
        }
        
        return schematics.get(schematicindex++);
    }
    
    public InitializedChest[] getChests() {
        return chests;
    }
    
    public Loadout[] getLoadouts() {
        return loadouts;
    }
    
    public LoadoutMenu getLoadoutMenu() {
        return menu;
    }
    
    private void unequipPlayer(Player player) {
        if (player == null || player.getWorld() == null) {
            return;
        }
        
        if (player.getWorld().getName().equals(worldname)) {
            player.getInventory().clear();
            ItemStack air = new ItemStack(Material.AIR);
            player.getEquipment().setHelmet(air);
            player.getEquipment().setChestplate(air);
            player.getEquipment().setLeggings(air);
            player.getEquipment().setBoots(air);
            player.getEquipment().setItemInHand(air);
            player.getInventory().setHeldItemSlot(player.getInventory().getHeldItemSlot() == 1 ? 5 : 1);
        }
    }
    
    @SuppressWarnings("deprecation")
    void equipLoadout(Player player, Loadout loadout) {
        GamePlayer gp = getPlayer(player);
        
        if (gp == null) {
            return;
        }
        
        gp.loadout = loadout;
        unequipPlayer(player);
        
        switch (gp.team) {
        case RED:
            player.teleport(random(redspawns).toLocation(getWorld()));
            break;
        case BLUE:
            player.teleport(random(bluespawns).toLocation(getWorld()));
            break;
        }
        
        if (loadout != null) {
            loadout.checkEquip(player);
        }
        
        ItemStack teamhat;
        
        switch (gp.team) {
        case RED:
            teamhat = new MaterialData(Material.WOOL, (byte) 14).toItemStack(1);
            break;
        case BLUE:
            teamhat = new MaterialData(Material.WOOL, (byte) 11).toItemStack(1);
            break;
        default:
            teamhat = new ItemStack(Material.WOOL);
            break;
        }
        
        player.getEquipment().setHelmet(teamhat);
    }
    
    @SuppressWarnings("deprecation")
    private void equipPlayer(GamePlayer gp, Player player, boolean initial) {
        switch (gp.team) {
        case RED:
            player.teleport(random(redspawns).toLocation(getWorld()));
            break;
        case BLUE:
            player.teleport(random(bluespawns).toLocation(getWorld()));
            break;
        }
        
        Loadout loadout = gp.loadout;
        unequipPlayer(player);
        player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 20, 100, true));
        player.addPotionEffect(new PotionEffect(PotionEffectType.SATURATION, 20, 100, true));
        
        if (loadout != null) {
            loadout.equip(player, initial);
        }
        
        ItemStack teamhat;
        
        switch (gp.team) {
        case RED:
            teamhat = new MaterialData(Material.WOOL, (byte) 14).toItemStack(1);
            break;
        case BLUE:
            teamhat = new MaterialData(Material.WOOL, (byte) 11).toItemStack(1);
            break;
        default:
            teamhat = new ItemStack(Material.WOOL);
            break;
        }
        
        player.getEquipment().setHelmet(teamhat);
        
        if (!scoreboard.equals(player.getScoreboard())) {
            player.setScoreboard(scoreboard);
        }
    }
    
    boolean addPlayer(Player player, Team team, long timejoin) {
        TeamDeathmatchConfig config = TeamDeathmatch.getInstance().getConfiguration();
        
        // Reject them if we are in WORLDGEN and this arena doesn't have a lobby configured
        if (GamePhase.WORLDGEN.equals(phase) && (!lobbyenabled || getWorld() == null)) {
            player.sendMessage(config.getMessages().formatJoinFailWorldgen());
            return false;
        }
        
        if (players.size() >= max) {
            if (players.size() >= privileged) {
                // Both limits have been exceeded
                player.sendMessage(config.getMessages().formatJoinHardFull());
                return false;
            } else if (TeamDeathmatch.getInstance().hasPermission(player, privilege)) {
                // Allow them to exceed the regular limit
            } else {
                // Do not allow them to exceed the regular limit
                player.sendMessage(config.getMessages().formatJoinSoftFull());
                return false;
            }
        }
        
        GamePlayer gp = new GamePlayer(player.getName(), team, timejoin);
        boolean added = players.add(gp);
        
        if (added) {
            player.setScoreboard(scoreboard);
            
            if (GamePhase.WORLDGEN.equals(phase)) {
                unequipPlayer(player);
                player.teleport(lobby.toLocation(getWorld()));
                Bukkit.getPluginManager().callEvent(new ArenaUpdateEvent(this));
                joining.put(player.getName(), player.getInventory().getContents().clone());
                Bukkit.getScheduler().runTaskLater(TeamDeathmatch.getInstance(), () -> removejoining(player), JOINING_SECONDS * 20L);
            } else {
                equipPlayer(gp, player, true);
                Bukkit.getPluginManager().callEvent(new ArenaUpdateEvent(this));
                joining.put(player.getName(), player.getInventory().getContents().clone());
                Bukkit.getScheduler().runTaskLater(TeamDeathmatch.getInstance(), () -> removejoining(player), JOINING_SECONDS * 20L);
            }
            
            return true;
        } else {
            return false;
        }
    }
    
    public boolean addPlayer(Player player) {
        TeamDeathmatchConfig config = TeamDeathmatch.getInstance().getConfiguration();
        
        if (GamePhase.WORLDGEN.equals(phase) && (!lobbyenabled || getWorld() == null)) {
            player.sendMessage(config.getMessages().formatJoinFailWorldgen());
            return false;
        }
        
        if (players.size() >= max) {
            if (players.size() >= privileged) {
                // Both limits have been exceeded
                player.sendMessage(config.getMessages().formatJoinHardFull());
                return false;
            } else if (TeamDeathmatch.getInstance().hasPermission(player, privilege)) {
                // Allow them to exceed the regular limit
            } else {
                // Do not allow them to exceed the regular limit
                player.sendMessage(config.getMessages().formatJoinSoftFull());
                return false;
            }
        }
        
        GamePlayer gp = new GamePlayer(player.getName(), getNewPlayerTeam());
        boolean added = players.add(gp);
        
        if (added) {
            player.setScoreboard(scoreboard);
            
            if (GamePhase.WORLDGEN.equals(phase)) {
                unequipPlayer(player);
                player.teleport(lobby.toLocation(getWorld()));
                Bukkit.getPluginManager().callEvent(new ArenaUpdateEvent(this));
                joining.put(player.getName(), player.getInventory().getContents().clone());
                Bukkit.getScheduler().runTaskLater(TeamDeathmatch.getInstance(), () -> removejoining(player), JOINING_SECONDS * 20L);
            } else {
                equipPlayer(gp, player, true);
                Bukkit.getPluginManager().callEvent(new ArenaUpdateEvent(this));
                joining.put(player.getName(), player.getInventory().getContents().clone());
                menu.openMenu(player);
                Bukkit.getScheduler().runTaskLater(TeamDeathmatch.getInstance(), () -> removejoining(player), JOINING_SECONDS * 20L);
            }
            return true;
        } else {
            return false;
        }
    }
    
    void replacejoining(Player player, ItemStack[] items) {
        joining.replace(player.getName(), items);
    }
    
    private void removejoining(Player player) {
        if (hasPlayer(player)) {
            ItemStack[] items = joining.remove(player.getName());
            
            if (items != null) {
                Inventory inv = player.getInventory();
                
                for (int i = 0; i < items.length; i++) {
                    ItemStack item = inv.getItem(i);
                    
                    if (Objects.equals(items[i], item)) {
                        continue;
                    } else if (item == null || item.getType().equals(Material.AIR)) {
                        continue;
                    } else {
                        inv.setItem(i, items[i]);
                    }
                }
            }
        }
    }
    
    private Team getNewPlayerTeam() {
        class IntWrapper { int i = 0; }
        
        IntWrapper red = new IntWrapper(), blue = new IntWrapper();
        
        players.forEach(gp -> {
            switch (gp.team) {
            case RED:
                red.i++;
                break;
            case BLUE:
                blue.i++;
                break;
            }
        });
        
        if (red.i > blue.i) {
            return Team.BLUE;
        } else if (blue.i > red.i) {
            return Team.RED;
        } else {
            return random.nextBoolean() ? Team.RED : Team.BLUE;
        }
    }
    
    void removeAllPlayers() {
        TeamDeathmatchConfig config = TeamDeathmatch.getInstance().getConfiguration();
        
        for (GamePlayer gp : new HashSet<>(players)) {
            Player p = Bukkit.getPlayer(gp.name);
            
            if (p != null) {
                removePlayer(p);
                
                Location spawn = config.getGame().getServerSpawn().toLocation(Bukkit.getWorld(config.getGame().getSpawnWorld()));
                
                if (spawn != null) {
                    p.teleport(spawn);
                }
            }
        }
    }
    
    public boolean removePlayer(Player player) {
        boolean removed = false;
        
        for (Iterator<GamePlayer> it = players.iterator(); it.hasNext(); ) {
            GamePlayer gp = it.next();
            
            if (Objects.equals(player.getName(), gp.name)) {
                it.remove();
                removed = true;
                break;
            }
        }
        
        if (removed) {
            unequipPlayer(player);
            player.setScoreboard(Bukkit.getScoreboardManager().getMainScoreboard());
            Bukkit.getPluginManager().callEvent(new ArenaUpdateEvent(this));
            return true;
        } else {
            return false;
        }
    }
    
    public boolean hasPlayer(Player player) {
        boolean[] has = { false };
        
        players.forEach(gp -> {
            if (Objects.equals(gp.name, player.getName())) {
                has[0] = true;
            }}
        );
        
        return has[0];
    }
    
    GamePlayer getPlayer(Player player) {
        for (GamePlayer gp : players) {
            if (Objects.equals(gp.name, player.getName())) {
                return gp;
            }
        }
        
        return null;
    }
    
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        if (joining.containsKey(e.getPlayer().getName())) {
            e.setCancelled(true);
            return;
        }
    }
    
    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        if (e.getWhoClicked() instanceof Player) {
            if (joining.containsKey(e.getWhoClicked().getName())) {
                e.setCancelled(true);
                ((Player) e.getWhoClicked()).updateInventory();
                return;
            }
            
            if (hasPlayer((Player) e.getWhoClicked())) {
                if (SlotType.ARMOR.equals(e.getSlotType()) && e.getRawSlot() == 5) {
                    e.setCancelled(true);
                    ((Player) e.getWhoClicked()).updateInventory();
                }
            }
        }
    }
    
    public void onPlayerDropItem(PlayerDropItemEvent e) {
        if (joining.containsKey(e.getPlayer().getName())) {
            e.setCancelled(true);
            return;
        }
    }
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerQuit(PlayerQuitEvent e) {
        Player player = e.getPlayer();
        
        for (Iterator<GamePlayer> it = players.iterator(); it.hasNext(); ) {
            GamePlayer gp = it.next();
            
            if (player.getName().equals(gp.name)) {
                it.remove();
                
                TeamDeathmatchConfig config = TeamDeathmatch.getInstance().getConfiguration();
                Location spawn = config.getGame().getServerSpawn().toLocation(Bukkit.getWorld(config.getGame().getSpawnWorld()));
                
                if (spawn != null) {
                    player.teleport(spawn);
                }
                
                Bukkit.getPluginManager().callEvent(new ArenaUpdateEvent(this));
            }
        }
    }
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerTeleport(PlayerTeleportEvent e) {
        if (hasPlayer(e.getPlayer())) {
            if (respawning.remove(e.getPlayer().getName())) {
                return;
            }
            
            Bukkit.getScheduler().runTaskLater(TeamDeathmatch.getInstance(), () -> {
                if (e.getPlayer().getWorld().getName().equals(this.worldname)) {
                    return;
                }
                
                removePlayer(e.getPlayer());
            }, 10L);
        }
    }
    
    @SuppressWarnings("deprecation")
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
        Player damaged, damager;
        
        if (e.getEntity() instanceof Player && e.getDamager() instanceof Player) {
            damaged = (Player) e.getEntity();
            damager = (Player) e.getDamager();
        } else if (e.getEntity() instanceof Player && e.getDamager() instanceof Projectile) {
            damaged = (Player) e.getEntity();
            Projectile p = (Projectile) e.getDamager();
            
            if (p.getShooter() instanceof Player) {
                damager = (Player) p.getShooter();
            } else {
                return;
            }
        } else {
            return;
        }
        
        GamePlayer gdamaged = getPlayer(damaged);
        GamePlayer gdamager = getPlayer(damager);
        
        if (gdamaged == null && gdamager == null) {
            return;
        } else if (gdamaged == null || gdamager == null) {
            e.setCancelled(true);
            return;
        } else if (Objects.equals(gdamaged.team, gdamager.team)) {
            e.setCancelled(true);
            return;
        }
    }
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerRespawn(PlayerRespawnEvent e) {
        if (hasPlayer(e.getPlayer())) {
            if (GamePhase.WORLDGEN.equals(phase)) {
                e.setRespawnLocation(lobby.toLocation(getWorld()));
            } else {
                switch (getPlayer(e.getPlayer()).team) {
                case RED:
                    e.setRespawnLocation(random(redspawns).toLocation(getWorld()));
                    break;
                case BLUE:
                    e.setRespawnLocation(random(bluespawns).toLocation(getWorld()));
                    break;
                default:
                    return;
                }
            }
        }
    }
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerDeath(PlayerDeathEvent e) {
        if (!GamePhase.INPROGRESS.equals(phase)) {
            return;
        }
        
        Player player = e.getEntity();
        Player killer = player.getKiller();
        boolean hasplayer = hasPlayer(player);
        
        if (hasplayer) {
            if (killer != null && hasPlayer(killer)) {
                switch (getPlayer(killer).team) {
                case RED:
                    objective.getScore(RED_POINTS).setScore(++redpoints);
                    break;
                case BLUE:
                    objective.getScore(BLUE_POINTS).setScore(++bluepoints);
                    break;
                }
            }
            
            Iterable<ItemStack> drops = new ArrayList<>(e.getDrops());
            e.getDrops().clear();
            Location died = player.getLocation();
            World world = died.getWorld();
            
            for (ItemStack drop : drops) {
                world.dropItemNaturally(died, drop);
            }
            
            Team previous;
            Location loc;
            long timejoin = getPlayer(player).joined;
            respawning.add(player.getName());
            
            if (GamePhase.WORLDGEN.equals(phase)) {
                previous = getPlayer(player).team;
                PlayerUtil.respawn(player, loc = lobby.toLocation(getWorld()));
            } else {
                switch (previous = getPlayer(player).team) {
                case RED:
                    PlayerUtil.respawn(player, loc = random(redspawns).toLocation(getWorld()));
                    break;
                case BLUE:
                    PlayerUtil.respawn(player, loc = random(bluespawns).toLocation(getWorld()));
                    break;
                default:
                    return;
                }
            }
            
            respawning.remove(player.getName());
            Bukkit.getScheduler().runTaskLater(TeamDeathmatch.getInstance(), () -> respawnPlayer(getPlayer(player), player, loc, previous, timejoin), 1L);
        }
    }
    
    private void respawnPlayer(GamePlayer gp, Player player, Location loc, Team previous, long timejoin) {
        if (gp == null) {
            addPlayer(player, previous, timejoin);
            return;
        }
        
        if (GamePhase.WORLDGEN.equals(phase)) {
            respawning.add(gp.name);
            player.teleport(loc);
            respawning.remove(gp.name);
            unequipPlayer(player);
            player.teleport(lobby.toLocation(getWorld()));
            player.updateInventory();
        } else {
            respawning.add(gp.name);
            player.teleport(loc);
            respawning.remove(gp.name);
            equipPlayer(gp, player, false);
            player.updateInventory();
        }
    }
    
    
    @SafeVarargs
    private static <T> T random(T... ts) {
        return ts[random.nextInt(ts.length)];
    }
}
