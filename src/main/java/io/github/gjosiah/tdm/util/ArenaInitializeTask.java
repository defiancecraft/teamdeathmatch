package io.github.gjosiah.tdm.util;

import io.github.gjosiah.tdm.TeamDeathmatch;
import io.github.gjosiah.tdm.config.ArenaSign;
import io.github.gjosiah.tdm.config.JsonInventory;
import io.github.gjosiah.tdm.impl.Arena;
import io.github.gjosiah.tdm.impl.InitializedChest;

import java.util.Objects;
import java.util.Set;
import java.util.logging.Logger;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.block.Sign;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.Inventory;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

public class ArenaInitializeTask extends BukkitRunnable {

    private Arena arena;
    private Runnable runnable;
    private Logger log;
    private SchematicPasteTask task;
    
    public ArenaInitializeTask(Arena arena, Runnable callback) {
        this.arena = arena;
        this.runnable = callback;
        this.log = TeamDeathmatch.getInstance().getLogger();
        this.task = null;
    }
    
    private void initSigns() {
        World world = arena.getWorld();
        
        if (world == null) {
            TeamDeathmatch.getInstance().getLogger().severe("World was null after schematic pasting!?!?");
            return;
        }
        
        Set<ArenaSign> quitsigns = TeamDeathmatch.getInstance().getListener().getQuitSigns();
        Set<ArenaSign> kitsigns = TeamDeathmatch.getInstance().getListener().getKitSigns();
        
        for (ArenaSign quit : quitsigns) {
            if (Objects.equals(quit.getArena(), arena.getLabel())) {
                Block block = quit.toBlock(world);
                
                if (block.getState() instanceof Sign) {
                    Sign sign = (Sign) block.getState();
                    sign.setLine(0, "");
                    sign.setLine(1, "Click to");
                    sign.setLine(2, "leave the arena");
                    sign.setLine(3, "");
                    sign.update(true, false);
                } else {
                    TeamDeathmatch.getInstance().getLogger().warning("Quit sign configured for arena " + arena.getLabel() + " at (x, y, z) (" +
                            quit.getX() + ", " + quit.getY() + ", " + quit.getZ() + "), but no sign was found.");
                }
            }
        }
        
        for (ArenaSign kit : kitsigns) {
            if (Objects.equals(kit.getArena(), arena.getLabel())) {
                Block block = kit.toBlock(world);
                
                if (block.getState() instanceof Sign) {
                    Sign sign = (Sign) block.getState();
                    sign.setLine(0, "");
                    sign.setLine(1, "Click to");
                    sign.setLine(2, "change kits");
                    sign.setLine(3, "");
                    sign.update(true, false);
                } else {
                    TeamDeathmatch.getInstance().getLogger().warning("Kit sign configured for arena " + arena.getLabel() + " at (x, y, z) (" +
                            kit.getX() + ", " + kit.getY() + ", " + kit.getZ() + "), but no sign was found.");
                }
            }
        }
    }
    
    private void killEntities() {
        World world = arena.getWorld();
        
        if (world == null) {
            TeamDeathmatch.getInstance().getLogger().severe("World was null after schematic pasting!?!?");
            return;
        }
        
        if (task == null) {
            TeamDeathmatch.getInstance().getLogger().severe("Task was null after schematic pasting!?!?");
            return;
        }
        
        Vector initial = new Vector(task.xi, task.yi, task.zi);
        Vector distant = new Vector(task.xi + task.xo, task.yi + task.yo, task.zi + task.zo);
        
        for (Entity e : world.getEntities()) {
            if (!EntityType.DROPPED_ITEM.equals(e.getType())) {
                continue;
            } else {
                Vector entity = e.getLocation().toVector();
                
                if (initial.getX() <= entity.getX() && distant.getX() + 1 >= entity.getX()
                        && initial.getY() <= entity.getY() && distant.getY() + 1 >= entity.getY()
                        && initial.getZ() <= entity.getZ() && distant.getZ() + 1 >= entity.getZ()) {
                    e.remove();
                }
            }
        }
    }
    
    private void initChests() {
        World world = arena.getWorld();
        
        if (world == null) {
            TeamDeathmatch.getInstance().getLogger().severe("World was null after schematic pasting!?!?");
            return;
        }
        
        // Eternal daytime
        world.setTime(12000L);
        world.setGameRuleValue("doDaylightCycle", "false");
        
        InitializedChest[] chests = arena.getChests();
        
        for (InitializedChest chest : chests) {
            Block block = world.getBlockAt(chest.getLocation().toLocation(world));
            
            if (block.getState() instanceof Chest) {
                Chest c = (Chest) block.getState();
                Inventory inv = c.getInventory();
                
                for (JsonInventory.JsonInventorySlot slot : chest.getInventory()) {
                    if (slot.getIndex() >= inv.getSize()) {
                        continue;
                    }
                    
                    inv.setItem(slot.getIndex(), slot.getItem());
                }
            }
        }
    }
    
    public void run() {
        if (arena.isWorldLoaded()) {
            log.info("Initializing arena '" + arena.getLabel() + "' as its World (" + arena.getWorldName() + ") has been loaded.");
            World world = arena.getWorld();
            Location origin = arena.getOrigin().toLocation(world);
            int blockrate = TeamDeathmatch.getInstance().getConfiguration().getGame().getBlockRate();
            
            this.task = new SchematicPasteTask(origin, arena.getSchematic(), blockrate)
                    .callback(this::initChests)
                    .callback(this::killEntities)
                    .callback(this::initSigns);
            
            if (runnable != null) {
                task.callback(runnable);
            }
            
            try {
                cancel();
            } catch (Exception e) {
                // For some reason the task wasn't running after all
            }
            
            task.runTaskTimer(TeamDeathmatch.getInstance(), 1L, 1L);
        } else {
            log.info("Arena '" + arena.getLabel() + "' cannot initialize as its World (" + arena.getWorldName() + ") is not loaded.");
            log.info("  Another attempt will occur in 60 seconds.");
        }
    }
}
