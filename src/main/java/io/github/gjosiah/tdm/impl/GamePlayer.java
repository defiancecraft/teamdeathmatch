package io.github.gjosiah.tdm.impl;

import java.util.Objects;

public class GamePlayer {

    final String name;
    final Team team;
    final long joined;
    Loadout loadout;
    
    GamePlayer(String name, Team team, long time) {
        this.name = name;
        this.team = team;
        this.joined = time;
        this.loadout = null;
    }
    
    GamePlayer(String name, Team team) {
        this.name = name;
        this.team = team;
        this.joined = System.currentTimeMillis();
    }
    
    public boolean equals(Object o) {
        return o instanceof GamePlayer && Objects.equals(name, ((GamePlayer) o).name);
    }
    
    public int hashCode() {
        return Objects.hashCode(name);
    }
}
