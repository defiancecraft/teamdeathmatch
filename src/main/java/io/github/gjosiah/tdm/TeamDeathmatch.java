package io.github.gjosiah.tdm;

import com.archeinteractive.defiancetools.util.command.CommandListener;
import com.archeinteractive.defiancetools.util.command.CommandRegistry;
import io.github.gjosiah.tdm.config.ArenaConfig;
import io.github.gjosiah.tdm.config.TeamDeathmatchConfig;
import io.github.gjosiah.tdm.impl.ArenaCommands;
import io.github.gjosiah.tdm.impl.ArenaManager;
import io.github.gjosiah.tdm.impl.MasterListener;

import java.io.File;

import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class TeamDeathmatch extends JavaPlugin {

    public static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
    private static final String CONFIG_FILENAME = "config.json";
    private static TeamDeathmatch instance = null;
    
    private TeamDeathmatchConfig config;
    private ArenaManager arenas;
    private MasterListener listener;
    private Economy economy;
    private Permission permissions;
    
    public TeamDeathmatch() {
        this.config = null;
        this.arenas = null;
        this.listener = null;
        
        getDataFolder().mkdirs();
        TeamDeathmatch.instance = this;
    }
    
    public void onEnable() {
        this.config = TeamDeathmatchConfig.load(getConfigurationFile(), TeamDeathmatchConfig.class);
        this.arenas = new ArenaManager(this, config);
        this.listener = new MasterListener(this);
        
        getServer().getPluginManager().registerEvents(listener, this);
        
        for (ArenaConfig arena : config.getArenas()) {
            arenas.initArena(arena);
        }
        
        arenas.forEach(entry -> entry.getValue().initialize());
        
        CommandListener.setup(this);
        CommandRegistry.registerPlayerCommand(this, "schpaste", "teamdeathmatch.schematic.paste", ArenaCommands::schematicpaste);
        CommandRegistry.registerPlayerCommand(this, "tdm", "teamdeathmatch.tdm", ArenaCommands::tdm);
        CommandRegistry.registerUniversalCommand(this, "listworlds", "teamdeathmatch.listworlds", ArenaCommands::listworlds);
        CommandRegistry.registerUniversalCommand(this, "listarenas", "teamdeathmatch.listarenas", ArenaCommands::listarenas);
        CommandRegistry.registerUniversalCommand(this, "loadworld", "teamdeathmatch.loadworld", ArenaCommands::loadworld);
        
        if (getServer().getPluginManager().getPlugin("Vault") != null) {
            getLogger().info("Found Vault, initializing Economy & Permissions");
            
            RegisteredServiceProvider<Economy> economyRSP = getServer().getServicesManager().getRegistration(Economy.class);
            
            if (economyRSP != null) {
                this.economy = economyRSP.getProvider();
            } else {
                getLogger().severe("Could not hook Economy from Vault!");
            }
            
            RegisteredServiceProvider<Permission> permissionRSP = getServer().getServicesManager().getRegistration(Permission.class);
            
            if (permissionRSP != null) {
                permissions = permissionRSP.getProvider();
            } else {
                getLogger().severe("Could not hook Permissions from Vault!");
            }
        } else {
            getLogger().info("Could not find Vault!");
        }
    }
    
    public void onDisable() {
        if (arenas == null) {
            getLogger().warning("Disabling: The ArenaManager was never created!");
        } else {
            arenas.dispose();
        }
        
        TeamDeathmatch.instance = null;
    }
    
    public ArenaManager getArenas() {
        return arenas;
    }
    
    public TeamDeathmatchConfig getConfiguration() {
        return config;
    }
    
    public MasterListener getListener() {
        return listener;
    }
    
    public File getConfigurationFile() {
        return new File(getDataFolder(), CONFIG_FILENAME);
    }
    
    public boolean hasPermission(Player player, String perm) {
        if (perm == null || perm.isEmpty()) {
            return true;
        }
        
        if (permissions == null) {
            return player.hasPermission(perm);
        } else {
            return permissions.has(player, perm);
        }
    }
    
    public void awardCurrency(Player player, double amount) {
        if (economy != null) {
            economy.depositPlayer(player, amount);
        }
    }
    
    
    public static TeamDeathmatch getInstance() {
        return instance;
    }
}
